import mesa
import os
from mre.MreECollectDataModes import *
import csv
import pandas as pd
import json
from mre.MreMapMetadataSaver import MreMapMetadataSaver

class MreStatistician:
    def __init__(self, model, simulation_params, folder_name, enum_collect_data, config_dict):
        self._model = model
        self._folder_name = folder_name
        self._enum_collect_data = enum_collect_data
        if self._do_collect_basic_data():
            self._init_dir(folder_name)
            self._create_results_csv()
            self._collect_config_dict(config_dict)  # collect settings of simulation
            self._save_map_metadata(simulation_params["ascii_map"])

    """
    INIT
    """

    def _init_dir(self, folder_name):
        os.makedirs(folder_name)

    def new_rep(self, rep_num):
        self._rep_num = rep_num
        self.create_cycles_explored_csv()
        self.create_explored_first_csv()
        self.create_agent_move_csv()
        self.create_received_knowledge_csv()

    def _save_map_metadata(self, ascii_map):
        mms = MreMapMetadataSaver()
        file_path = os.path.join(self._folder_name, "map-metadata.json")
        mms.save_map_metadata(ascii_map, file_path)

    """
    SETTINGS VARIABLES
    """

    def _collect_config_dict(self, config_dict):
        data = config_dict
        file_path = os.path.join(self._folder_name, "config.json")
        i = 0
        with open(file_path, "w") as json_file:
            json_file.write("{\n")
            for key, value in data.items():
                edited_val = value
                if isinstance(value, bool):
                    if bool(value) == True:
                        edited_val = "true"
                    else:
                        edited_val = "false"
                elif isinstance(value, int):
                    edited_val = int(value)
                else:
                    edited_val = '"' + str(value) + '"'
                json_file.write(f'    "{key}": {edited_val}')
                if i < len(config_dict)-1:
                    json_file.write(f',')
                json_file.write(f'\n')
                i += 1
            json_file.write("}\n")

        """
        field_names = list(simulation_params.keys())
        with open(self._params_file_path, mode='w', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=field_names)
            writer.writeheader()
            writer.writerow(simulation_params)
        """

    def _do_collect_all_data(self):
        return self._enum_collect_data >= 2

    def _do_collect_basic_data(self):
        return self._enum_collect_data >= 1

    """
    CREATE FILES
    """

    def _create_results_csv(self):
        self._results_file_path = os.path.join(self._folder_name, "results.csv")
        header = ['rep', 'result']
        with open(self._results_file_path, 'w') as file:
            file.write(','.join(header) + '\n')

    def create_cycles_explored_csv(self):
        CYCLES_EXPLORED_FILE_NAME = "cycles_explored.csv"
        cycles_explored_file_name_rep = (str(self._rep_num) + "-" + CYCLES_EXPLORED_FILE_NAME)
        self._cycles_explored_file_path = os.path.join(self._folder_name, cycles_explored_file_name_rep)
        self._cycles_explored_df = pd.DataFrame(columns=['cycle', 'explored_num'])

    def create_explored_first_csv(self):
        EXPLORED_FIRST_FILE_NAME = "explored_first.csv"
        explored_first_file_name_rep = (str(self._rep_num) + "-" + EXPLORED_FIRST_FILE_NAME)
        self._explored_first_file_path = os.path.join(self._folder_name, explored_first_file_name_rep)
        self._explored_first_df = pd.DataFrame(columns=["cycle", "agent_id", "cell"])

    def create_agent_move_csv(self):
        AGENT_MOVE_FILE_NAME = "agent_move.csv"
        agent_move_file_name_rep = (str(self._rep_num) + "-" + AGENT_MOVE_FILE_NAME)
        self._agent_move_file_path = os.path.join(self._folder_name, agent_move_file_name_rep)
        self._agent_move_df = pd.DataFrame(columns=["cycle", "agent_id", "cell"])

    def create_received_knowledge_csv(self):
        RECEIVED_KNOWLEDGE_FILE_NAME = "received_knowledge.csv"
        received_knowledge_file_name_rep = (str(self._rep_num) + "-" + RECEIVED_KNOWLEDGE_FILE_NAME)
        self._received_knowledge_file_path = os.path.join(self._folder_name, received_knowledge_file_name_rep)
        self._received_knowlege_df = pd.DataFrame(columns=["cycle", "from_id", "to_id"])

    """
    END OF SIM
    """

    def inform_simulation_run_rep_finished(self, total_num_of_cycles):
        if self._do_collect_basic_data():
            self.write_results(total_num_of_cycles)
        if self._do_collect_all_data():
            self.write_cycles_explored_file()
            self.write_explored_first_file()
            self.write_agent_move_file()
            self.write_received_knowledge_file()

    def write_cycles_explored_file(self):
        file_path = self._cycles_explored_file_path
        df = self._cycles_explored_df
        df.to_csv(file_path, index=False)

    def write_explored_first_file(self):
        file_path = self._explored_first_file_path
        df = self._explored_first_df
        df.to_csv(file_path, index=False)

    def write_agent_move_file(self):
        file_path = self._agent_move_file_path
        df = self._agent_move_df
        df.to_csv(file_path, index=False)

    def write_received_knowledge_file(self):
        file_path = self._received_knowledge_file_path
        df = self._received_knowlege_df
        df.to_csv(file_path, index=False)

    def write_results(self, total_num_of_cycles):
        row = [self._rep_num, total_num_of_cycles]
        with open(self._results_file_path, mode='a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(row)

    """
    EVENTS
    """

    def inform_explored_and_cycle_stat(self, num_explored_free_cells, num_of_cycles):
        if self._do_collect_basic_data():
            #print(f"stats: {self._rep_num}: num_explored_free_cells: {num_explored_free_cells} num_of_cycles: {num_of_cycles}")
            row = [num_of_cycles, num_explored_free_cells]
            self._cycles_explored_df.loc[len(self._cycles_explored_df)] = row

    def inform_agent_explored_first_stat(self, agent_id, cell, sim_cycle):
        if self._do_collect_all_data():
            #print(f"stats: agent {agent_id} explored first cell {cell} at {sim_cycle}")
            row = [sim_cycle, agent_id, f"{cell[0]}-{cell[1]}"]
            self._explored_first_df.loc[len(self._explored_first_df)] = row

    def inform_agent_moved_stat(self, agent_id, cell, sim_cycle):
        # provides info that agent with agent id moved to cell new_cell in sim_cycle cycle
        if self._do_collect_all_data():
            #print(f"stats: agent {agent_id} moved to cell {cell} at {sim_cycle}")
            row = [sim_cycle, agent_id, f"{cell[0]}-{cell[1]}"]
            self._agent_move_df.loc[len(self._agent_move_df)] = row

    def inform_agents_received_knowledge_stat(self, sim_cycle, from_id, to_id):
        if self._do_collect_all_data():
            #print(f"stats: agent {to_id} received info from {from_id} at {sim_cycle}")
            row = [sim_cycle, from_id, to_id]
            self._received_knowlege_df.loc[len(self._received_knowlege_df)] = row
