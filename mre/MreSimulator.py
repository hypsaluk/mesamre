import copy
from mre.grids.MreEnvGrid import *
from mre.MreAgent import *
import re

class MreSimulator:

    def __init__(self, simulation_params, random_param, model):
        # init variables
        self._simulation_params = simulation_params
        self._random = random_param
        self._sim_cycles = 0
        self._agents = []
        self._model = model
        self._max_cycles_num = simulation_params["width"] ** 3
        # prepare scenario objects
        self._controller = self._prepare_controller_object(simulation_params)
        agent_grid = self._prepare_agent_grid_object(simulation_params)
        # init simulation
        self._init_simulation(simulation_params, agent_grid, model)
        # start of 0. sim cycle
        #self.inform_explored_and_cycle()
        #self.update_cycle()
        self._wake_up_agents()

    """ 
    INIT 
    """

    def _init_simulation(self, simulation_params, agent_grid, model):
        # 1) agents
        self._init_agents(simulation_params["num_agents"], simulation_params["agent_vision_r"],
                          simulation_params["agent_comm_r"],
                          agent_grid, model)
        # 2) environment grid
        self._init_env_grid(simulation_params["width"], simulation_params["height"], simulation_params["ascii_map"])

    def _init_agents(self, num_agents, r_v, r_c, agent_grid, model):
        for i in range(num_agents):  # create agents
            new_grid_instance = copy.deepcopy(agent_grid)
            new_agent = MreAgent(i, model, new_grid_instance, r_v, r_c)
            self._agents.append(new_agent)

    def _try_place_agents_by_init_position(self, agents):
        x = None
        y = None
        try:
            initial_position = self._simulation_params["initial_position"]
            initial_position = str(initial_position)
            pattern = r'\d+'
            digits = re.findall(pattern, initial_position)
            coordinates = [int(digit) for digit in digits]
            #print(f"tuple and tuple type and tuple len: {coordinates} {type(coordinates)} {len(coordinates)}")
            x = int(coordinates[0])
            y = int(coordinates[1])
        except Exception as e:
            return False

        if isinstance(x, int) and isinstance(y, int):
            self._env_grid.place_agents_deterministic_init_pos(agents, (x, y))
            return True
        return False

    def _init_env_grid(self, width, height, ascii_map):
        # create blank grid
        self._env_grid = MreEnvGrid(width, height, self, ascii_map, self._random.randrange, self._simulation_params)
        if not self._try_place_agents_by_init_position(self._agents):
            self._env_grid.place_agents_random_position(self._agents)  # place agents into grid random
        self._env_grid.set_simulator(self)

    def _prepare_controller_object(self, simulation_params):
        return simulation_params["controller_class"](self._random.randrange, simulation_params)

    def _prepare_agent_grid_object(self, simulation_params):
        return simulation_params["agent_grid_class"](simulation_params["width"], simulation_params["height"],
                                                     simulation_params)
    def _wake_up_agents(self):
        self._agents_sense()  # agents get sense first data about env
        self._agents_control()  # agents plan first move

    """
    SIMULATION CYCLE
    """

    def _agents_sense(self):
        self._env_grid.sense_action()  # provide sensed data to agents

    def _agents_control(self):
        self._controller.control(self._env_grid, self)

    def _agents_move(self):
        self._env_grid.move_action(self)

    def update_cycle(self):
        self._sim_cycles += 1
        #print(f"sim cycle updated to value {self._sim_cycles}")

    def step(self):
        self._agents_move()  # agents move
        """  END OF SINGLE SIM CYCLE  """
        self.inform_explored_and_cycle_simulator()
        self.update_cycle()
        """  START OF NEW SIM CYCLE  """
        self._agents_sense()  # agents get data about environment
        if self.get_exploration_finished_bool():  #or (self.get_number_sim_cycle() > self._max_cycles_num):
            self.inform_explored_and_cycle_simulator()
            return
        self._agents_control()  # agents 1) exchange information 2) and coordinate

    """
    LOGICS
    """

    def get_exploration_finished_bool(self):
        return self._env_grid.get_num_free_not_explored_cells() <= 0

    """
    STATISTICS
    """

    def inform_explored_and_cycle_simulator(self):
        num_explored_free_cells = self.get_num_free_explored_cells()
        num_of_cycles = self.get_number_sim_cycle()
        self._model.inform_explored_and_cycle_model(num_explored_free_cells, num_of_cycles)

    def inform_agent_explored_first_simulator(self, agent_id, sensed_cell):
        self._model.inform_agent_explored_first_model(agent_id, sensed_cell, self.get_number_sim_cycle())

    def inform_agent_moved_simulator(self, agent_id, new_cell):
        self._model.inform_agent_moved_model(agent_id, new_cell, self.get_number_sim_cycle())

    def inform_agents_received_knowledge_simulator(self, from_id, to_id):
        self._model.inform_agents_received_knowledge_model(self.get_number_sim_cycle(), from_id, to_id)

    """
    GETTERS
    """

    def get_num_free_explored_cells(self):
        return self._env_grid.get_num_free_explored_cells()

    def get_number_sim_cycle(self):
        return self._sim_cycles

    def get_agents(self):
        return self._agents

    def get_env_grid(self):  # used by visualizations
        return self._env_grid
