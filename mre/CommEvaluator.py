from scipy.stats import poisson


class CommEvaluator:

    def __init__(self):
        pass

    def get_knowledge_received_array_basic(self, env_grid):
        return self._get_knowledge_received_array(env_grid, self._get_received_bool_basic)

    def get_knowledge_received_array_poisson(self, env_grid):
        return self._get_knowledge_received_array(env_grid, self._get_received_bool_poisson)

    def _get_knowledge_received_array(self, env_grid, received_bool_fc):
        # generates a 2D array of bools where array[from(agent_1_id)][to(agent_2_id)] tells whether a2 received knowledge from a1
        agents = env_grid.get_agents()
        received_array = [[False] * len(agents) for _ in range(len(agents))]
        for a_from in agents:
            for a_to in agents:
                if a_from.unique_id == a_to.unique_id:
                    continue
                received = received_bool_fc(env_grid, a_from, a_to)
                received_array[a_from.unique_id][a_to.unique_id] = received
        return received_array

    def _get_received_bool_basic(self, env_grid, a_from, a_to):
        received = False
        distance = env_grid.get_manhattan_distance(a_from.pos, a_to.pos)
        comm_r = a_from.get_comm_range()
        if distance <= comm_r:
            received = True
        return received

    def _get_received_bool_poisson(self, env_grid, a_from, a_to):
        distance = env_grid.get_manhattan_distance(a_from.pos, a_to.pos)
        comm_r = a_from.get_comm_range()  # average range of vertices for a message
        if distance > comm_r:  # agents shall not hear each other over cr
            return False
        poisson_distr = poisson(mu=comm_r)
        message_reach = poisson_distr.rvs()  # how far particular message went
        received = message_reach >= distance
        #print(f"(message_reach {message_reach}){a_to.unique_id} received from {a_from.unique_id} on distance {distance} with comm range {comm_r}: {received}")
        return received
