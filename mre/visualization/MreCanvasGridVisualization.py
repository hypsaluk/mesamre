from mesa_viz_tornado.modules.CanvasGridVisualization import *


class MreCanvasGrid(CanvasGrid):
    """
    Subclass of Mesa canvas grid
        - allows to specify which grid will be rendered (in parent, model.grid is always rendered)
        - portrayal_methods_dict (dict):
            key (int) - index of layer
            value (dict) - one portrayal_method for given layer
    """

    def __init__(
            self,
            grid_function_name,
            grid_function_kwargs,
            portrayal_method,
            grid_width,
            grid_height,
            canvas_width=500,
            canvas_height=500,
    ):
        super().__init__(portrayal_method, grid_width, grid_height, canvas_width, canvas_height)
        self.grid_function_name = grid_function_name
        self.grid_function_kwargs = grid_function_kwargs

    def get_grid(self, model):
        grid_function = getattr(model, self.grid_function_name)
        grid = grid_function() if self.grid_function_kwargs is None else grid_function(self.grid_function_kwargs)
        return grid

    def render(self, model):
        """
            Renders grid on canvas in explorer
            For each cell renders 1) agent or cell square 2) text layer
        """
        grid = self.get_grid(model)
        grid_state = defaultdict(list)
        for x in range(grid.width):
            for y in range(grid.height):
                cell_content_dict = grid.get_cell_content_dict((x, y))
                portrayal_dict = self.portrayal_method(cell_content_dict, x, y)

                for layer, portrayal in portrayal_dict.items():
                    portrayal["x"] = x
                    portrayal["y"] = y
                    grid_state[layer].append(portrayal)

        return grid_state
