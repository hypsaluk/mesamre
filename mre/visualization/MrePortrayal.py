from mre.grids.cell_properties.MreECellStatus import *
from mre.grids.cell_properties.MreECellOccupancy import *
from mre.grids.cell_properties.MreCellText import *
from mre.grids.cell_properties.MreEAgentsAvatars import *


class MrePortrayal:
    """ GENERAL """
    _rect_prayl = {
        "Shape": "rect",
        "Filled": "true",
        "Layer": 1,
        "Color": "#AAAAAA",
        "w": 1,
        "h": 1,
        "xAlign": 0,
        "yAlign": 0,
    }

    _circ_prayl = {
        "Shape": "circle",
        "Filled": "true",
        "r": 0.5,
        "Color": "Pink",
    }

    @staticmethod
    def general_text_prayl(cell_content, x, y):
        text_dict = {}  #cell_content[CELL_TEXT_NAME]
        text = f"({x}, {y}) "
        for key, val in text_dict.items():
            text = text + key + ": " + val + "; "
        coords_text_prayl = {
            "text": text,
            "text_color": "Black",
        }
        return coords_text_prayl

    """ MODEL """

    @staticmethod
    def modelg_agent_prayl(cell_content, x, y):
        # Agent in cell -> draw agent
        if len(cell_content["agent"]) > 0:
            prayl = {
                "Shape": "circle",
                "Filled": "true",
                "r": 0.5,
                "Color": "Pink",
            }
            return prayl
        return None

    @staticmethod
    def model_cell_prayl(cell_content, x, y):
        color = "yellow"
        prayl = MrePortrayal._rect_prayl.copy()
        occupancy = cell_content[CELL_OCCUPANCY_PROP_ENAME]
        status = cell_content[CELL_STATUS_PROP_ENAME]

        if len(cell_content["agent"]) > 0:
            color = "Black"
        else:
            if occupancy == MreECellOccupancy.FREE.value:
                if status == MreECellStatus.UNKNOWN.value:
                    color = "#8BC34A"
                elif status == MreECellStatus.DISCOVERED.value:
                    color = "#BFE6A8"
                elif status == MreECellStatus.EXPLORED.value:
                    color = "#cfdb46"

            else:
                if status == MreECellStatus.UNKNOWN.value:
                    color = "#FF5722"
                elif status == MreECellStatus.DISCOVERED.value:
                    color = "#FFCCBC"

        prayl["Color"] = color
        return prayl

    @staticmethod
    def model_grid_portrayl_methods(cell_content, x, y):
        """
        the function returns dict key => val, where key is layer and val is portrayal for given layer
        """
        pralys_dict = {}

        prayls_fcs_list = [
            MrePortrayal.general_text_prayl,
            MrePortrayal.modelg_agent_prayl,
            MrePortrayal.model_cell_prayl,
        ]

        # for every prayl fc obtain prayl and register it under layer key
        layer = len(prayls_fcs_list)
        for prayl_fc in prayls_fcs_list:
            prayl = prayl_fc(cell_content, x, y)
            if prayl is None:
                continue
            pralys_dict[layer] = prayl
            layer = layer - 1

        return pralys_dict

    """
    AGENTS
    """

    @staticmethod
    def agentg_agent_prayl(cell_content, x, y):
        # Agent in cell -> draw agent
        prayl = None
        avatar_val = cell_content[AGENTS_AVATARS_PROP_ENAME]
        if avatar_val == MreEAgentsAvatars.SELF.value:
            prayl = {
                "Shape": "circle",
                "Filled": "true",
                "r": 0.5,
                "Color": "Pink",
            }
        elif MreEAgentsAvatars.is_an_agent_id_val(avatar_val):
            prayl = {
                "Shape": "circle",
                "Filled": "true",
                "r": 0.5,
                "Color": "Black",
            }
        return prayl

    @staticmethod
    def agentg_cell_prayl(cell_content, x, y):
        color = "yellow"
        prayl = MrePortrayal._rect_prayl.copy()
        occupancy = cell_content[CELL_OCCUPANCY_PROP_ENAME]
        status = cell_content[CELL_STATUS_PROP_ENAME]

        if occupancy == MreECellOccupancy.FREE.value:
            if status == MreECellStatus.UNKNOWN.value:
                color = "white"  # "#8BC34A"
            elif status == MreECellStatus.DISCOVERED.value:
                color = "#BFE6A8"
            elif status == MreECellStatus.EXPLORED.value:
                color = "#aec24a"

        else:
            if status == MreECellStatus.UNKNOWN.value:
                color = "white"  # "#FF5722"
            elif status == MreECellStatus.DISCOVERED.value:
                color = "#FFCCBC"
            elif status == MreECellStatus.EXPLORED.value:
                color = "black"
        #  elif not disc and not free:
        # color = "#FFCCBC" # red light

        prayl["Color"] = color
        return prayl

    @staticmethod
    def agent_grid_portrayl_methods(cell_content, x, y):
        """
        the function returns dict key => val, where key is layer and val is portrayal for given layer
        """
        pralys_dict = {}

        prayls_fcs_list = [
            MrePortrayal.general_text_prayl,
            MrePortrayal.agentg_agent_prayl,
            MrePortrayal.agentg_cell_prayl,
        ]

        # for every prayl fc obtain prayl and register it under layer key
        layer = len(prayls_fcs_list)
        for prayl_fc in prayls_fcs_list:
            prayl = prayl_fc(cell_content, x, y)
            if prayl is None:
                continue
            pralys_dict[layer] = prayl
            layer = layer - 1

        return pralys_dict
