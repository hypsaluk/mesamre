from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder


def revert_y_in_coords(grid, coords):
    x = coords[0]
    y = grid.height - coords[1] - 1
    return x, y


class MrePathFinder:
    """
    MrePathFinder serves for finding paths in binary grids.
    """

    def __init__(self, mre_grid):
        self._mre_grid = mre_grid

    def get_astar_path_pf(self, start, end, binary_matrix_for_pathfinding):
        """
            input:
                start: (x ,y) of start
                end: (x ,y) of end
                grid: instance of MreMultiGrid in which the path is to be computed
                is_cell_accessible_fc: maps cell to bool, True if agent can access the cell
            output:
                list of coords representing the path, where each path[i] and path[i+1] are neighbours
                path[0] is start cell and path[len-1] is end cell
        """
        # create 0-1 matrix from grid (where 1 = free and 0 = occupied)
        pf_grid = Grid(matrix=binary_matrix_for_pathfinding)
        finder = AStarFinder()
        # y in input has to be inverted
        start = revert_y_in_coords(self._mre_grid, start)
        end = revert_y_in_coords(self._mre_grid, end)
        start_node = pf_grid.node(start[0], start[1])
        end_node = pf_grid.node(end[0], end[1])
        # self._print_info_before(start_node, end_node, mx)
        # find path
        path, second = finder.find_path(start_node, end_node, pf_grid)
        # rever back the y axis
        reverted_path = self._a_star_output_to_grid_path_list(path)
        # self._print_info_after(path, reverted_path, second, start, end)
        return reverted_path

    def _a_star_output_to_grid_path_list(self, input_list):
        """
        input: list of nodes from pathfinding package
        output: list of (x, y) coords with reverted y-axis
        """
        reverted_list = []
        for node in input_list:
            coords = (node.x, node.y)
            reverted_coords = revert_y_in_coords(self._mre_grid, coords)
            reverted_list.append(reverted_coords)
        return reverted_list
