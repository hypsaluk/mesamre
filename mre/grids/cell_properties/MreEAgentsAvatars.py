from enum import Enum

AGENTS_AVATARS_PROP_ENAME = "agents_avatars"


class MreEAgentsAvatars(Enum):
    NONE = -2
    SELF = -1

    @staticmethod
    def is_an_agent_id_val(val):
        try:
            int_val = int(val)
            if int_val >= 0:
                return True
        except ValueError:
            return False
        return False
