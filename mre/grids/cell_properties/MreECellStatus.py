from enum import Enum

CELL_STATUS_PROP_ENAME = "cell_status"


class MreECellStatus(Enum):
    UNKNOWN = 0
    DISCOVERED = 1
    EXPLORED = 2
