from enum import Enum

CELL_OCCUPANCY_PROP_ENAME = "cell_occupancy"


class MreECellOccupancy(Enum):
    FREE = 0
    OBSTACLE = 1
