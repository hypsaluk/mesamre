import numpy as np
import mesa
from mesa.space import *
import math

from mre.grids.cell_properties.MreCellText import *
from mre.grids.cell_properties.MreECellOccupancy import *
from mre.grids.MrePathFinder import *
from mre.grids.cell_properties.MreECellStatus import *


class MreGrid(MultiGrid):
    """
    Grid used for Mre exploration
    - it has two principal property layers - occupancy and discovered

    Attributes:
        _rand_fc (function): a function generating random int from <0, i) range

    Public methods:
        get_cell_content_dict(cell) - for given cell returns info about agents and cells properties
        get_cell_prop_val(cell, property) - returns specific property of given cell
        count_astar_path(start, target, collisions, discoveredness) - returns the shortest path from start cell to target cell
    """

    """
    INIT
    """

    def __init__(self, width, height):
        super().__init__(width, height, False)
        self._add_prop_layers()  # add property layers to grid

    def _add_prop_layers(self):
        """
        creates blank property layers
        - occupancy layer: defines if the cell is free or occupied
        - discovered layer: defines whether the cell is already discovered by any agent
        - text layer: auxiliary layer for saving info about the cell by model/grid/agent
        """
        # create prop layers
        self.prop_lay_cell_occupancy = mesa.space.PropertyLayer(CELL_OCCUPANCY_PROP_ENAME, self.width,
                                                                self.height, MreECellOccupancy.OBSTACLE.value,
                                                                dtype=int)
        self.prop_lay_discovered = mesa.space.PropertyLayer(CELL_STATUS_PROP_ENAME, self.width, self.height,
                                                            MreECellStatus.UNKNOWN.value, dtype=int)

        dict_str_str_dtype = type(np.dtype({'names': ['key', 'value'], 'formats': [np.str_, np.str_]}))
        dif_basic_val = {}
        """
        self.prop_lay_text = mesa.space.PropertyLayer(CELL_TEXT_NAME, self.width, self.height,
                                                      dif_basic_val,
                                                      dtype=dict_str_str_dtype)
        """

        # register prop layers to main grid
        self.add_property_layer(self.prop_lay_cell_occupancy)
        self.add_property_layer(self.prop_lay_discovered)
        #self.add_property_layer(self.prop_lay_text)

    """
    GETTERS - NONTRIVIAL
    """

    def get_cell_agents(self, cell):
        x, y = cell
        grid_content = self._grid[x][y]  # returns agents in cell
        return grid_content

    def get_cell_prop_val(self, pos, prop_name):
        """
        For cell on given position returns value of given property
        """
        layer = self.properties[prop_name]
        if layer is None:
            return None
        x, y = pos
        val = layer.data[x][y]  # return value in given layer on coords x, y
        return val

    def get_cell_content_dict(self, pos):
        """
        Returns a dictionary with the content of cell - agent and cell parameters
        """
        x, y = pos
        cell_content = {}
        # get info about agent
        agent = self.get_cell_agents(pos)
        cell_content["agent"] = agent
        # get info about cell properties
        property_layers = self.properties
        for layer_name, layer in property_layers.items():
            cell_content[layer_name] = layer.data[x][y]
        return cell_content

    """
    LOGICS
    """

    """
    def get_euclidian_distance(self, cell1, cell2):
        x1, y1 = cell1
        x2, y2 = cell2
        x_diff = abs(x2 - x1)
        y_diff = abs(y2 - y1)
        euclidian_distance = math.sqrt((x_diff ** 2) + (y_diff ** 2))
        return euclidian_distance
    """

    def get_manhattan_distance(self, cell_1, cell_2):
        x1 = cell_1[0]
        y1 = cell_1[1]
        x2 = cell_2[0]
        y2 = cell_2[1]
        x_diff = abs(x2 - x1)
        y_diff = abs(y2 - y1)
        manhattan_distance = x_diff + y_diff
        return manhattan_distance

    """
    GETTERS
    """

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    """ occupancy """

    def is_cell_free(self, cell):
        return self.get_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME) == MreECellOccupancy.FREE.value

    def is_cell_obstacle(self, cell):
        return self.get_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME) == MreECellOccupancy.OBSTACLE.value

    """ status """

    def is_cell_unknown(self, cell):
        return self.get_cell_prop_val(cell, CELL_STATUS_PROP_ENAME) == MreECellStatus.UNKNOWN.value

    def is_cell_discovered(self, cell):
        return self.get_cell_prop_val(cell, CELL_STATUS_PROP_ENAME) == MreECellStatus.DISCOVERED.value

    def is_cell_explored(self, cell):
        return self.get_cell_prop_val(cell, CELL_STATUS_PROP_ENAME) == MreECellStatus.EXPLORED.value

    """ 
    SETTERS
    """

    """ occupancy """

    def set_cell_free(self, cell):
        self.prop_lay_cell_occupancy.set_cell(cell, MreECellOccupancy.FREE.value)

    def set_cell_obstacle(self, cell):
        self.prop_lay_cell_occupancy.set_cell(cell, MreECellOccupancy.OBSTACLE.value)

    """ status """

    def set_cell_unknown(self, cell):
        self.prop_lay_discovered.set_cell(cell, MreECellStatus.UNKNOWN.value)

    def set_cell_discovered(self, cell):
        self.prop_lay_discovered.set_cell(cell, MreECellStatus.DISCOVERED.value)

    def set_cell_explored(self, cell):
        self.prop_lay_discovered.set_cell(cell, MreECellStatus.EXPLORED.value)

    def set_cell_prop_val(self, cell, prop_name, val):
        """
        For cell on given position sets value of given property
        """
        layer = self.properties[prop_name]
        if layer is None:
            return
        layer.set_cell(cell, val)
