from mre.grids.MreGrid import *
from mre.MreMapManager import *
from mre.grids.cell_properties.MreECellStatus import *
import random


class MreEnvGrid(MreGrid):
    """
    Grid used for representing the environment, inherits from MreGrid.

    For each cell, it holds info about discoveredness and occupancy.

    It manages agents
    - places them on the beginning of simulation
    - sense action - sends data to agents they sense based on their given position
    - move action - moves with agents based on their next intensioned move

    Attributes:
        _model (mre.MreModel): Model which holds this env grid
        _free_cells_list (list): auxiliary list containing coords of all cells which occupancy is free
        _not_free_cells_list (list): auxiliary list containing coords of all cells which occupancy is not free
    """

    def __init__(self, width, height, model, ascii_map, rand_fc, scenario_params):
        super().__init__(width, height)
        self._rand_fc = rand_fc
        self._model = model
        self._free_cells_list = []
        self._not_free_cells_list = []
        self._init_occupancy_from_asciimap(ascii_map)  # init cells properties based on ASCII map
        self._scenario_params = scenario_params

    def set_simulator(self, simulator):
        self._simulator = simulator

    """
    INIT ENV SPACE
    """

    def _init_occupancy_from_asciimap(self, asciimap):
        """
        sets values of property layer for occupancy based on 2D ascii map
        """
        y = (self.height - 1)
        for line in asciimap:
            if y < 0:  # end of grid
                break
            x = 0
            for c in line:
                if (x >= self.width) or (c == '\n'):
                    break  # end of grid line
                cell_props = MreMapManager.trans_map_char_to_cell_props(c)
                occupancy_value = cell_props[CELL_OCCUPANCY_PROP_ENAME]
                self._set_occupancy_to_cell(x, y, occupancy_value)  # the function to init single cell
                x = x + 1
            y = y - 1

    def _set_occupancy_to_cell(self, x, y, occupancy_value):
        """
            - sets value of occupancy property to cell (x, y)
            - adds cell to list of free or unfree cells
        """
        self.prop_lay_cell_occupancy.set_cell((x, y), occupancy_value)
        if occupancy_value is MreECellOccupancy.FREE.value:
            self._free_cells_list.append((x, y))
        else:
            self._not_free_cells_list.append((x, y))

    """
    AGENTS PLACE
    """

    def _place_agents(self, agents, position):
        # generate list of neighbouring agents positions
        for i in range(len(agents)):
            self.place_agent(agents[i], position)  # put agent into
            self.move_agent(agents[i], position)  # to init agents position
        return
        diameter = 0
        fr = None
        to = None
        for x in self._free_cells_list:
            for y in self._free_cells_list:
                dist = self.get_astar_distance(x, y)
                print(f"comparing {x} and {y}")
                if diameter < dist:
                    diameter = dist
                    fr = x
                    to = y
                    print(f"new diameter: {diameter}")
        print(f"diameter: {diameter}")
        print(f"fr: {fr} - to: {to}")


    def place_agents_random_position(self, agents):
        """ places agents into initial position in grid; on random spot, as neighbours """
        rand_free_cell_index = self._rand_fc(len(self._free_cells_list))
        rand_free_cell = self._free_cells_list[rand_free_cell_index]
        self.place_agents_deterministic_init_pos(agents, rand_free_cell)

    def place_agents_deterministic_init_pos(self, agents, initial_position):
        # places first agent on init pos, each other agent tries to place next to last agent most N, W, S, E
        x, y = initial_position
        if not self.out_of_bounds(initial_position):
            if self.is_cell_free(initial_position):
                #positions = self._generate_deterministic_positions(initial_position, len(agents))
                #print(f"positions: {positions}")
                self._place_agents(agents, initial_position)
                return
            else:
                print(f"The initial position ({x}, {y}) is not a free cell")
        else:
            print(f"The initial position ({x}, {y}) is out of bounds")

    def _generate_deterministic_positions(self, initial_position, agents_num):
        positions = [initial_position]
        """
        TODO place other agents as close as possible to agent 0
        """
        # take position 0
        # for neighbourhood 1,2,3...
        nbr_range = 1
        while len(positions) < agents_num:
            new_positions = []
            nbrs = self.get_neighborhood(initial_position, False, False, nbr_range)
            for nbr in nbrs:
                # if adjacent to any position in positions and not in positions
                if self.is_cell_free(nbr):
                    if (nbr not in positions) and (nbr not in new_positions):
                        # if adjacent to any position in positions
                        nbr_nbrs = self.get_neighborhood(nbr, False, False, 1)
                        for pos in positions:
                            if pos in nbr_nbrs:
                                # add to positions
                                new_positions.append(nbr)
                                #print(f"{pos}")
                                break
                if (len(positions)+len(new_positions)) >= agents_num:
                    break
            positions.extend(new_positions)
            nbr_range += 1
            #print(f"positions: {positions}")

        return positions

    """ AGENTS MOVE """

    def move_action(self, simulation):
        """ takes 'next moves' from agents and based on it moves with them; if move is not possible, agents are
        staying"""
        agents = self._model.get_agents()
        agents_num = len(agents)
        indexes_list = list(range(agents_num))
        for i in indexes_list:
            agent = agents[i]
            #print(f"MOVING AGENT {agent.unique_id}")
            # compute new position of agent
            next_move = agent.get_move()
            if next_move is None:
                continue
            self._try_move_agent(next_move, simulation, agent)

    def _try_move_agent(self, next_move, simulation, agent):
        x = agent.pos[0] + next_move[0]
        y = agent.pos[1] + next_move[1]
        new_pos = (x, y)
        agent_may_move_to_cell = self._get_agent_may_move_to_cell_bool(new_pos, agent)
        if agent_may_move_to_cell:  # cell accessible -> shift agent
            self.move_agent(agent, new_pos)
            simulation.inform_agent_moved_simulator(agent.unique_id, new_pos)  # send statistics
        else:  # agent couldnt move into the cell
            # provide id of agent into whom he bumped
            bumped_agents = self.get_cell_agents(new_pos)
            if len(bumped_agents) <= 0:
                agent.set_collision_occured(-1)
            else:
                b_agent = bumped_agents[0]
                id_bumped_agent = b_agent.unique_id
                agent.set_collision_occured(id_bumped_agent)

    def _get_agent_may_move_to_cell_bool(self, cell, agent):
        """
        return True, if agent can step on given cell
        """
        # check if occupied
        return not self.is_cell_obstacle(cell)  # agent cant move on occupied cell

    """ 
    AGENT SENSE 
    """

    def sense_action(self):
        """ provide sensed data to agents based on their current positions """
        for agent in self.get_agents():
            self._agent_explore(agent)
            self._agent_discover(agent)

    def _agent_explore(self, agent):
        explored_cell = agent.pos
        agent.receive_explored_cell(explored_cell)
        self._update_explored_cell(explored_cell, self._simulator, agent)

    def _update_explored_cell(self, explored_cell, simulation, agent):
        if not self.is_cell_explored(explored_cell):  # agent explored the cell for the first time -> inform stats
            simulation.inform_agent_explored_first_simulator(agent.unique_id, explored_cell)
        self.set_cell_explored(explored_cell)

    def _agent_discover(self, agent):
        discovered_cells = self.get_neighborhood(agent.pos, False)
        discovered_info = self._get_agent_discovered_info(discovered_cells)
        agent.receive_discovered_info(discovered_info)  # provide the data to agent
        self._update_discovered_cells(discovered_cells)

    def _get_agent_discovered_info(self, discovered_cells):
        discovered_info = {}  # dict cell -> occupancy
        for cell in discovered_cells:
            cell_occupancy = self.get_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME)
            discovered_info[cell] = cell_occupancy
        return discovered_info

    def _update_discovered_cells(self, discovered_cells):
        for cell in discovered_cells:
            if self.is_cell_unknown(cell):
                self.set_cell_discovered(cell)

    """
    GETTERS
    """

    def get_agents(self):
        return self._model.get_agents()

    def get_num_free_explored_cells(self):
        num = 0
        for cell in self._free_cells_list:
            if self.is_cell_explored(cell):
                num += 1
        return num

    def get_num_free_not_explored_cells(self):
        num = 0
        for cell in self._free_cells_list:
            if not self.is_cell_explored(cell):
                num += 1
        return num

    """
        PATH FINDING
        """

    def get_astar_distance(self, start, end):
        """
        Returns shortest path from start to end in grid, found by astar algorithm
        """
        path = self.get_astar_path(start, end)
        return len(path)

    def get_astar_path(self, start, end):
        """
        Returns shortest path from start to end in grid, found by astar algorithm
        """
        binary_matrix_for_pathfinding = self._prepare_binary_matrix_for_path_finding()
        mre_path_finder = MrePathFinder(self)
        path = mre_path_finder.get_astar_path_pf(start, end, binary_matrix_for_pathfinding)
        return path

    def is_cell_accessible(self, cell):
        """
        return True, if agent can step on given cell
        = if cell is known to be free
        """
        if self.is_cell_free(cell):
            return True
        return False

    def _prepare_binary_matrix_for_path_finding(self):
        """ prepares binary matrix free-unfree based on grid and is_cell_accessible function  """
        matrix = []
        free_val = 1
        unfree_val = 0
        for y in range(self.height):
            row = []
            for x in range(self.width):
                pos = (x, self.height - y - 1)  # MESA y-axis is inverted
                cell_occ_val = unfree_val
                if self.is_cell_accessible(pos):
                    cell_occ_val = free_val
                row.append(cell_occ_val)
            matrix.append(row)
        return matrix
