from mre.grids.MreGrid import *
from mre.grids.cell_properties.MreEAgentsAvatars import *
import math


class MreAgentGrid(MreGrid):
    """
    Local grid owned by a single agent.
    Displays discovered cells and their occupancy.
    """

    def __init__(self, width, height, scenario_params):
        super().__init__(width, height)
        self.init_layers()
        self.agent_last_pos = None

    def init_layers(self):
        prop_lay_agents_avatars = mesa.space.PropertyLayer(AGENTS_AVATARS_PROP_ENAME,
                                                           self.width,
                                                           self.height,
                                                           MreEAgentsAvatars.NONE.value,
                                                           dtype=int)
        self.add_property_layer(prop_lay_agents_avatars)

    """ RECEIVE ENV INFO """

    def mark_explored_cell(self, explored_cell):
        expl_status_val = MreECellStatus.EXPLORED.value
        free_occupancy_val = MreECellOccupancy.FREE.value
        self.properties[CELL_STATUS_PROP_ENAME].set_cell(explored_cell, expl_status_val)
        self.properties[CELL_OCCUPANCY_PROP_ENAME].set_cell(explored_cell, free_occupancy_val)
        self.mark_self_pos(explored_cell)  # mark self position in map

    def mark_discovered_info(self, discovered_info):
        for cell, occupancy in discovered_info.items():
            disc_status_val = MreECellStatus.DISCOVERED.value
            if self.is_cell_unknown(cell):
                self.properties[CELL_STATUS_PROP_ENAME].set_cell(cell, disc_status_val)
            self.properties[CELL_OCCUPANCY_PROP_ENAME].set_cell(cell, occupancy)

    """
        def receive_env_info(self, cells_list):
            for cell_info in cells_list:
                x = cell_info["x"]
                y = cell_info["y"]
                cell = (x, y)
                occupancy = cell_info[CELL_OCCUPANCY_PROP_ENAME]
                status = cell_info[CELL_STATUS_PROP_ENAME]
                # mark occupancy of cell
                self.properties[CELL_OCCUPANCY_PROP_ENAME].set_cell((x, y), occupancy)
                # mark that the cell is discovered
                if not (self.is_cell_explored(cell) and (
                        status == MreECellStatus.DISCOVERED.value)):  # do not overwrite explored to discovered
                    self.properties[CELL_STATUS_PROP_ENAME].set_cell((x, y), status)
    """

    def mark_self_pos(self, pos):
        if self.agent_last_pos is not None:
            self.set_cell_prop_val(self.agent_last_pos, AGENTS_AVATARS_PROP_ENAME, MreEAgentsAvatars.NONE.value)
        self.agent_last_pos = pos
        self.set_cell_prop_val(pos, AGENTS_AVATARS_PROP_ENAME, MreEAgentsAvatars.SELF.value)

    """
    PATH FINDING
    """

    def get_astar_distance(self, start, end):
        """
        Returns shortest path from start to end in grid, found by astar algorithm
        """
        path = self.get_astar_path(start, end)
        return len(path)

    def get_astar_path(self, start, end):
        """
        Returns shortest path from start to end in grid, found by astar algorithm
        """
        binary_matrix_for_pathfinding = self._prepare_binary_matrix_for_path_finding()
        mre_path_finder = MrePathFinder(self)
        path = mre_path_finder.get_astar_path_pf(start, end, binary_matrix_for_pathfinding)
        return path

    def is_cell_accessible(self, cell):
        """
        return True, if agent can step on given cell
        = if cell is known to be free
        """
        if not self.is_cell_unknown(cell):
            if self.is_cell_free(cell):
                return True
        return False

    def _prepare_binary_matrix_for_path_finding(self):
        """ prepares binary matrix free-unfree based on grid and is_cell_accessible function  """
        matrix = []
        free_val = 1
        unfree_val = 0
        for y in range(self.height):
            row = []
            for x in range(self.width):
                pos = (x, self.height - y - 1)  # MESA y-axis is inverted
                cell_occ_val = unfree_val
                if self.is_cell_accessible(pos):
                    cell_occ_val = free_val
                row.append(cell_occ_val)
            matrix.append(row)
        return matrix
