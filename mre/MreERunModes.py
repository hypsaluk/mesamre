from enum import Enum

RUN_MODES_ENAME = "run_modes"


class MreERunModes(Enum):
    BROWSER = 0
    TERMINAL = 1
