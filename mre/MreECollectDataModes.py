from enum import Enum

COLLECT_DATA_MODES_ENAME = "run_modes"


class MreECollectDataModes(Enum):
    NONE = 0
    BASIC = 1
    EVENTS = 2
