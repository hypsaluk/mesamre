from mre.grids.cell_properties.MreECellStatus import *
from mre.grids.cell_properties.MreECellOccupancy import *
import copy
from mre.FriendKnowledge import *
from mre.grids.MreEnvGrid import *
from mre.CommEvaluator import *

class MreController:
    """
    The class controlling agents behavior. It takes care of inter-agent communication and planning next move.
    """

    def __init__(self, rand_function, scenario_params):
        self._rand_function = rand_function
        self._scenario_params = scenario_params
        self._comm_eval = scenario_params["comm_eval"]
        self._comm_evaluator = CommEvaluator()

    def control(self, env_grid, simulator):
        pass

    """
    FRONTIERS
    """

    def _is_a_frontier(self, cell, grid):
        is_frontier = grid.is_cell_free(cell) and grid.is_cell_discovered(cell)
        return is_frontier

    def get_frontier_cells(self, grid):
        # a frontier cell is such a cell, which is discovered-free
        width = grid.width
        height = grid.height
        frontiers = []
        for y in range(height):
            for x in range(width):
                cell = (x, y)
                if self._is_a_frontier(cell, grid):
                    frontiers.append(cell)
        return frontiers

    """
    COMMUNICATION ESTABLISHING
    """

    def _get_knowledge_from_agent(self, agent):
        knowledge = FriendKnowledge()
        knowledge.id = agent.unique_id
        knowledge.position = agent.pos
        knowledge.grid = agent.get_grid()
        knowledge.target_point = agent.get_target_cell()
        return knowledge

    def _get_knowledge_received_by_agent(self, senders, receiver_id, received_array):
        # input: array[sender_id][receiver_id]
        received_knowledge_list = []
        for sender in senders:
            if sender.unique_id == receiver_id:
                continue
            receive_bool = received_array[sender.unique_id][receiver_id]
            if receive_bool:
                knowledge = self._get_knowledge_from_agent(sender)
                received_knowledge_list.append(knowledge)
        return received_knowledge_list

    """ 
    SHARE KNOWLEDGE 
    """

    """
    def share_knowledge_basic(self, agents):
        """""" shares knowledge of explored and discovered cells among agents """"""
        common_grid = agents[0].get_grid()
        for i in range(2):
            for a in agents:
                agent_grid = a.get_grid()
                self._merge_grid_cells_basic(common_grid, agent_grid)

    def _merge_grid_cells_basic(self, grid1, grid2):
        """"""
            each grid has following layers
            - occupancy
            - status
        """"""
        X = grid1.width
        Y = grid1.height

        # iterate over each cell
        for y in range(Y):
            for x in range(X):
                cell = (x, y)
                # if cell in grid1 has lower status than in grid2 -> overwrite
                if self._has_first_higher_status(cell, grid2, grid1):
                    self._overwrite_cell_grids(cell, grid1, grid2)
                else:
                    self._overwrite_cell_grids(cell, grid2, grid1)
        return grid1

    def _has_first_higher_status(self, cell, grid_1, grid_2):
        """"""
        decides whether cell in grid1 has higher status than in grid2
        """"""
        if grid_1.is_cell_explored(cell) and (not grid_2.is_cell_explored(cell)):
            return True
        elif grid_1.is_cell_discovered(cell) and (grid_2.is_cell_unknown(cell)):
            return True
        return False

    def _overwrite_cell_grids(self, cell, grid_wrong, grid_good):
        """"""
        overwrite cell in grid wrong by cell from grid good
        """"""
        status = grid_good.get_cell_prop_val(cell, CELL_STATUS_PROP_ENAME)
        occupancy = grid_good.get_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME)
        grid_wrong.set_cell_prop_val(cell, CELL_STATUS_PROP_ENAME, status)
        grid_wrong.set_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME, occupancy)
        return grid_wrong
    """

    """
    def get_nearest_frontier(self, grid, cell):
        frontiers = self.find_frontier_cells(grid)
        if len(frontiers) < 1:
            return None
        nearest_frontier = frontiers[0]
        nf_distance = grid.get_astar_distance(cell, nearest_frontier)
        for frontier in frontiers:
            frontier_distance = grid.get_astar_distance(cell, frontier)
            # if frontier is nearer than actual nf
            if frontier_distance < nf_distance:
                nearest_frontier = frontier
                nf_distance = frontier_distance
        return nearest_frontier
    """







    """
    def get_communication_clusters(self, env_grid):
        """"""
        splits agents into communication clusters based on their positions and communication range
        """"""
        clusters = []
        agents = env_grid.get_agents()
        for agent in agents:
            # first agent creates first cluster
            if len(clusters) <= 0:
                clusters.append([agent])
                continue
            # second and other agents:
            # count what all cells are covered
            pos = agent.pos
            comm_r = agent.get_comm_range()
            covered_cells = self._count_communication_covered_cells(pos, comm_r, env_grid)
            # print(f"cells covered by {agent.unique_id}: {covered_cells}")
            agent_placed_in_cluster = False
            # for each cell, if an agent is in it, connect to communication cluster
            for seen_cell in covered_cells:
                if agent_placed_in_cluster:
                    break
                seen_agents = env_grid.get_cell_agents(seen_cell)
                # if seen agent -> try to find the seen agent in cluster
                for seen_agent in seen_agents:
                    if agent_placed_in_cluster:
                        break
                    if seen_agent is not None:
                        # iterate over clusters and find seen_agent
                        for cluster in clusters:
                            if agent_placed_in_cluster:
                                break
                            if seen_agent in cluster:
                                # add to cluster where seen_agent
                                cluster.append(agent)
                                agent_placed_in_cluster = True
                        if agent_placed_in_cluster:
                            break
            # agent has not been placed in a cluster -> place in a new cluster
            if not agent_placed_in_cluster:
                new_cluster = [agent]
                clusters.append(new_cluster)
        return clusters

    def _count_communication_covered_cells(self, pos, comm_r, env_grid):
        covered_cells = []
        last_wave = [pos]
        for i in range(comm_r):
            new_wave = []
            for cell in last_wave:  # opec cells in given wave
                nbrs = env_grid.get_neighborhood(cell, False, False, 1)  # get nbrs
                for nbr in nbrs:
                    if env_grid.is_cell_free(  # if nbr is free and yet not opened -> add
                            nbr) and nbr not in covered_cells and nbr not in last_wave and nbr not in new_wave:
                        # new covered cell -> add
                        new_wave.append(nbr)
            covered_cells = covered_cells + last_wave
            last_wave = new_wave
        covered_cells = covered_cells + last_wave
        return covered_cells
    """
