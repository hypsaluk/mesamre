from mesa import Agent
from mre.grids.MreAgentGrid import *


class MreAgent(Agent):
    """
        An agent from multi-agent exploration scenario, extending the base mesa.Agent class from mesa module.
        note: The MreAgent class shall be independent of the model as much as possible. For that, there is effort
        not to use self.model attributes or methods when other way is possible.

        Attributes:
            _rand_function (function): A function which takes positive int i and returns random  int from range <0, i)
                (range including zero, but excluding the i)
            _vision_r (float): The vision range of agent in cells units
            _comm_r (float): The communication range of agent in cells units
            _target_cell (coords): The target cell agent wants to visit
            _next_move ((int, int)): The next move agent wants to do
            _local_grid (MreAgentGrid): The private, local grid map of agent. Serves for building its local
                                        map and marking local information into it.

        Public methods:
            receive_env_info(cells_list) -> void: the input channel of agent, with which he can receive the information
                                                   about environment (by sensing or communication)
            get_move() -> move: returns the move which agent wants to do in the following time step
    """

    def __init__(self, unique_id, model, grid, vision_r, comm_r):
        super().__init__(unique_id, model)
        # assign from constructor
        self._rand_function = model.random.randrange
        self._vision_r = vision_r
        self._comm_r = comm_r
        self._target_cell = None
        self._next_move = None
        # init grid
        self._local_grid = grid
        self._collision_occured = None
        self.collision_occured_number = 0

    """ INPUT - SENSORS SENSING """

    def receive_explored_cell(self, explored_cell):
        self._local_grid.mark_explored_cell(explored_cell)

    def receive_discovered_info(self, discovered_info):
        self._local_grid.mark_discovered_info(discovered_info)

    """
    SETTERS
    """

    def set_next_move(self, new_next_move):
        self._next_move = new_next_move

    """
    def set_agent_grid(self, new_agent_grid):
        self._local_grid = new_agent_grid
    """

    def set_target_cell(self, new_target_cell):
        """ Assigns to agent the grid cell agent wants to visit """
        self._target_cell = new_target_cell

    def set_collision_occured(self, val):
        self._collision_occured = val

    """
    GETTERS
    """

    """
    def get_vision_range(self):
        return self._vision_r
    """

    def get_comm_range(self):
        return self._comm_r

    def get_target_cell(self):
        return self._target_cell

    def get_grid(self):
        return self._local_grid

    def get_move(self):
        """ return the move which agent wants to do in following time step """
        return self._next_move

    def get_collision_occured(self):
        return self._collision_occured

    def get_after_collision_move(self, agent_dodge):
        move = self.get_move()
        # prepare or dodge
        banned_move = self.get_move()
        nbrs = self.get_grid().get_neighborhood(self.pos, False, False, 1)
        if agent_dodge == 1:  # do deterministic dodge
            for nbr in nbrs:
                if nbr != move:
                    target_nbr = nbr
                    move = (target_nbr[0] - self.pos[0], target_nbr[1] - self.pos[1])
        elif agent_dodge == 2:  # do random dodge
            rnd_index = self._rand_function(len(nbrs))
            nbr = nbrs[rnd_index]
            move = (nbr[0] - self.pos[0], nbr[1] - self.pos[1])
            while len(nbrs) >= 2 and (banned_move[0] == move[0] and banned_move[1] == move[1]):
                rnd_index = self._rand_function(len(nbrs))
                nbr = nbrs[rnd_index]
                move = (nbr[0] - self.pos[0], nbr[1] - self.pos[1])
        return move
