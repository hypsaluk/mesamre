import os
from mre.grids.cell_properties.MreECellOccupancy import *


class MreMapManager:

    """
    The class which takes care of reading maps from file system and helps transforming them into model grid.
    - provides function for reading ASCII map from file
    - provides mapping function from ASCII char to cell properties
    """
    def __init__(self, maps_dir):
        self.maps_dir = maps_dir

    def get_ascii_map_from_file(self, path):
        full_path = os.path.join(self.maps_dir, path)
        content_2d_array = []
        with open(full_path, "r") as file:
            # Iterate over each line in the file
            for line in file:
                content_2d_array.append(line)
        return content_2d_array

    @staticmethod
    def trans_map_char_to_cell_props(char):
        def_val = {
            CELL_OCCUPANCY_PROP_ENAME: MreECellOccupancy.FREE.value
        }
        switcher = {
            'F': {
                CELL_OCCUPANCY_PROP_ENAME: MreECellOccupancy.FREE.value
            },
            'O': {
                CELL_OCCUPANCY_PROP_ENAME: MreECellOccupancy.OBSTACLE.value
            },
            '1': {
                CELL_OCCUPANCY_PROP_ENAME: MreECellOccupancy.FREE.value
            },
            '0': {
                CELL_OCCUPANCY_PROP_ENAME: MreECellOccupancy.OBSTACLE.value
            },
        }

        dict = switcher.get(char, def_val)
        return dict
