from mre.MreSimulator import *
from _datetime import datetime

class MreModel(mesa.Model):
    """
    Model including the main grid (model grid) and agents.
    With step() method it controls the simulation process.
    """

    def __init__(self, simulation_params, statistician):
        super().__init__()
        self._statistician = statistician
        self._simulator = MreSimulator(simulation_params, self.random, self)  # simulator
        self.running = True  # mesa run model
        current_datetime = datetime.now()
        print("simulation rap started at: ", end="")
        print(current_datetime.strftime("%d-%m %H:%M:%S"))

    """
    STEP FUNCTION
    """

    def step(self):
        print(str(self._simulator.get_number_sim_cycle()) + " " + str(datetime.now().time()))
        print(str(self.get_num_free_explored_cells()) + " explored; it remains " + str(self.get_num_free_not_explored_cells()))
        self._simulator.step()
        if self._simulator.get_exploration_finished_bool():
            self.running = False
            total_num_of_cycles = self._simulator.get_number_sim_cycle()
            self._statistician.inform_simulation_run_rep_finished(total_num_of_cycles)
            print(f"total number of cycles: {total_num_of_cycles}")

    """
    STATISTICS
    """

    def inform_explored_and_cycle_model(self, num_explored_free_cells, num_of_cycles):
        self._statistician.inform_explored_and_cycle_stat(num_explored_free_cells, num_of_cycles)

    def inform_agent_explored_first_model(self, agent_id, sensed_cell, num_sim_cycles):
        self._statistician.inform_agent_explored_first_stat(agent_id, sensed_cell, num_sim_cycles)

    def inform_agent_moved_model(self, agent_id, new_cell, num_sim_cycles):
        self._statistician.inform_agent_moved_stat(agent_id, new_cell, num_sim_cycles)

    def inform_agents_received_knowledge_model(self, sim_cycle, from_id, to_id):
        self._statistician.inform_agents_received_knowledge_stat(sim_cycle, from_id, to_id)

    """
    GETTERS
    """

    def get_agents(self):
        return self._simulator.get_agents()

    def get_env_grid(self):  # used by visualizations
        return self._simulator.get_env_grid()

    def get_agent_by_agent_id(self, agent_id):
        agents = self._simulator.get_agents()
        for agent in agents:
            if agent.unique_id == agent_id:
                return agent
        return None

    def get_agent_grid(self, agent_id):
        agent = self.get_agent_by_agent_id(agent_id)
        agent_grid = agent.get_grid()
        return agent_grid

    def get_num_free_explored_cells(self):
        return self._simulator.get_env_grid().get_num_free_explored_cells()

    def get_num_free_not_explored_cells(self):
        return self._simulator.get_env_grid().get_num_free_not_explored_cells()
