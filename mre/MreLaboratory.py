import mesa

from mre.visualization.MreCanvasGridVisualization import *
from mre.visualization.MrePortrayal import *
from datetime import datetime
from mre.MreModel import *
from mre.MreController import *
from mre.grids.MreAgentGrid import *
from mre.MreERunModes import *
from mre.MreStatistician import *
import inspect
import time


class MreLaboratory:
    """
    Laboratory receives simulation configuration, controller and agent grid.
    It prepares Model and Server, and starts the simulation run.
    Model contains the data and logic of simulation. It also collects data about simulation and exports them into files.
    Server is running in browser on localhost. It provides graphics and step() button. The server inherently creates model inside.
    """

    def __init__(self, config_dict, controller_class, agent_grid_class):
        self._controller_class = controller_class
        self._agent_grid_class = agent_grid_class
        self._set_default_params()
        self._overrite_default_by_scenario_params(config_dict)
        self._config_dict = config_dict

    def _set_default_params(self):
        """
        Sets default values to the simulation parameters.
        """

        # simulation params
        self._num_agents = 5
        self._agent_vision_r = 1
        self._agent_comm_r = 1
        self._agent_vision_r = 1
        self._maps_dir = "scenarios/maps"
        self._map_name = "36_grid.txt"
        self._num_of_vertices = 0
        self._scenario_name = "default scenario"
        self._collect_data = False
        # visualisations
        self._cell_pixels = 20
        # run params
        self._run_mode = MreERunModes.BROWSER.value
        self._num_of_reps = 1
        self._results_folder_path = "scenarios/"

    def _overrite_default_by_scenario_params(self, params_dict):
        """
        Sets given simulation parameters.
        """
        for key, value in params_dict.items():
            attr_name = '_' + key
            setattr(self, attr_name, value)

    def provide_visualization_elements(self, model_grid_width, model_grid_height, model_grid_canvas_w,
                                       model_grid_canvas_h):
        """
        Returns list of visualization elements which are drawn by server during simulation.
        """
        visualization_elements = []

        """ texts """
        """
        text_fc = (lambda model: f"Num of not discovered cells: {model.get_num_free_not_discovered_cells()}")
        visualization_elements.append(text_fc)
        text_fc = (lambda model: f"Num of agents: {self._num_agents}")
        visualization_elements.append(text_fc)
        """

        """ env grid """
        canvas_model_grid = MreCanvasGrid("get_env_grid", None,
                                          MrePortrayal.model_grid_portrayl_methods,
                                          model_grid_width, model_grid_height,
                                          model_grid_canvas_w, model_grid_canvas_h)
        visualization_elements.append(canvas_model_grid)

        """ agents grids """
        for i in range(self._num_agents):
            agent_id = i
            agent_model_grid = MreCanvasGrid("get_agent_grid", agent_id,
                                             MrePortrayal.agent_grid_portrayl_methods,
                                             model_grid_width, model_grid_height,
                                             model_grid_canvas_w, model_grid_canvas_h)
            visualization_elements.append(agent_model_grid)

        # chart
        """
        num_discovered_chart = mesa.visualization.ChartModule(
            [{"Label": "num_free_discovered_cells", "Color": "Black"}])
        visualization_elements.append(num_discovered_chart)
        """
        return visualization_elements

    def provide_model_and_vis(self):
        """
            returns server with initialized model inside it; server is ready for .launch()
        """
        # acquire ascii map
        map_manager = MreMapManager(self._maps_dir)
        self._ascii_map = map_manager.get_ascii_map_from_file(self._map_name)
        # width and height (in cells) of model grid is set based on size of acquired ascii map
        model_grid_width = len(self._ascii_map[0])-1
        model_grid_height = len(self._ascii_map)
        print(f"model_grid_width: {model_grid_width}")
        print(f"model_grid_height: {model_grid_height}")
        self._model_grid_canvas_w = model_grid_width * self._cell_pixels
        self._model_grid_canvas_h = model_grid_height * self._cell_pixels
        # visualizations
        visualization_elements = self.provide_visualization_elements(model_grid_width, model_grid_height,
                                                                     self._model_grid_canvas_w,
                                                                     self._model_grid_canvas_h)

        # create simulation run directory
        # model arguments

        self.prepare_sim_run_id()
        self.prepare_folder_path()
        self._count_num_of_vertices()

        attributes_dict = {}
        attributes = vars(self)  # Get all attributes and their values
        for attr_name, attr_value in attributes.items():
            attr_name = attr_name[1:]
            attributes_dict[attr_name] = attr_value

        simulation_params = {
            # agents
            "num_agents": self._num_agents,
            "agent_comm_r": self._agent_comm_r,
            "agent_grid_class": self._agent_grid_class,
            "agent_vision_r": self._agent_vision_r,
            # env
            "width": model_grid_width,
            "height": model_grid_height,
            "map_name": self._map_name,
            "ascii_map": self._ascii_map,
            # scenario
            "controller_class": self._controller_class,
            "scenario_name": self._scenario_name,
            # simulation run
            "sim_run_id": self._sim_run_id,
            "collect_data": self._collect_data,
            "folder_path": self._folder_path,
            "num_of_vertices": self._num_of_vertices,
        }
        params = attributes_dict
        params.update(simulation_params)

        return params, visualization_elements

    def prepare_sim_run_id(self):
        map_name = self._map_name[:-4]  # remove txt
        num_agents = "a" + str(self._num_agents)
        comm_range = "cr" + str(self._agent_comm_r)
        r_f = "rf" + str(self._r_f)
        mut = "um_" + str(self._u_multiplicative_constant)
        mof = "ofm_" + str(self._of_multiplicative_constant)
        num_reps = str(self._num_of_reps)

        current_datetime = datetime.now()
        time_id = "[" + current_datetime.strftime("%H-%M-%S") + "]"

        sim_run_id = num_agents + "-" + comm_range + "-" + r_f
        if self._r_f > 0:
            sim_run_id = sim_run_id + "-" + mut + "-" + mof
        sim_run_id = sim_run_id + "-" + map_name + "-" + num_reps + "-" + time_id
        "-" + str(time_id)
        self._sim_run_id = sim_run_id

    def prepare_folder_path(self):
        res_folder_path = self._results_folder_path
        sim_run_id = self._sim_run_id
        folder_path = os.path.join(res_folder_path, sim_run_id)
        self._folder_path = folder_path

    def _init_statistician(self, simulation_params):
        full_folder_path = self._folder_path
        self._statistician = MreStatistician(self, simulation_params, full_folder_path, self._collect_data, self._config_dict)

    def _count_num_of_vertices(self):
        map_manager = MreMapManager(self._maps_dir)
        num_of_vertices = 0
        for line in self._ascii_map:
            for c in line:
                if c == '\n':
                    continue
                occupancy = map_manager.trans_map_char_to_cell_props(c)[CELL_OCCUPANCY_PROP_ENAME]
                if occupancy == MreECellOccupancy.FREE.value:
                    num_of_vertices += 1
            # print(f"c occupancy: {c} {occupancy}")
        # print(f"num of vertices: {num_of_vertices}")
        self._num_of_vertices = num_of_vertices

    def run(self):
        # create the server which runs on a port in browser
        simulation_params, visualization_elements = self.provide_model_and_vis()
        model_class = MreModel
        model_kwargs = {
            "simulation_params": simulation_params,
        }

        self._init_statistician(simulation_params)  # statistician
        model_kwargs["statistician"] = self._statistician

        if self._run_mode == MreERunModes.BROWSER.value:  # in browser
            self._statistician.new_rep(0)
            server = mesa.visualization.ModularServer(
                model_class,
                visualization_elements,
                "MRE Simulation",
                model_kwargs,
            )
            self.server = server
            self.server.launch()
        elif self._run_mode == MreERunModes.TERMINAL.value:  # in terminal, more reps
            if simulation_params["initial_position"] != "(-1,-1)":
                for i in range(self._num_of_reps):
                    print(f"rep num: {i}")
                    self._statistician.new_rep(i)
                    model = MreModel(simulation_params, self._statistician)
                    while model.running:
                        model.step()
            else:
                print(f'mode: examine each free cell')
                # count all free cells
                ascii_map = simulation_params["ascii_map"]
                free_cells_list = self.get_list_free_cells(ascii_map)
                print(f"free_cells_list:{free_cells_list}")

                # for each cell do _num_of_reps repetitions
                for i in range(self._num_of_reps):
                    for cell in free_cells_list:
                        rep_num = (i * len(free_cells_list))+free_cells_list.index(cell)
                        #print(cell)
                        print(f"cell: {cell}, {free_cells_list.index(cell)+1}. from {len(free_cells_list)}")
                        print(f"rep num: {rep_num}")
                        simulation_params["initial_position"] = f"({cell[0]},{cell[1]})"
                        self._statistician.new_rep(rep_num)
                        model = MreModel(simulation_params, self._statistician)
                        start_time = time.time()
                        while model.running:
                            model.step()
                        print("Function execution time:", time.time() - start_time)


    def get_list_free_cells(self, ascii_map):
        free_cells_list = []
        for i in range(len(ascii_map)):  # iterate over each line
            line = ascii_map[len(ascii_map) - i - 1]
            for j in range(len(line)):  # iterate over each char
                c = line[j]
                if c == "F":
                    print(f"({j}, {i})", end="")
                    cell = (j, i)
                    free_cells_list.append(cell)
            print()
        return free_cells_list
