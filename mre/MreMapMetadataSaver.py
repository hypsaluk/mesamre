

class MreMapMetadataSaver:
    def __init__(self):
        pass

    def save_map_metadata(self, ascii_map, file_path):
        num_free_cells = 0
        for line in ascii_map:
            for c in line:
                if c == 'F':
                    num_free_cells += 1
        # save number of free cells
        # save diameter
        # save branch factor
        with open(file_path, "w") as file:
            # Write a line to the file
            file.write(f"num_free_cells: {num_free_cells}")