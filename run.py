import sys
from mre.MreLaboratory import *

""" get scenario objects """
scenario_folder = sys.argv[1]  # get scenario folder path
# move to scenario folder
sys.path.append(scenario_folder)
config_file_name = "config.json"
config_path = os.path.join(scenario_folder, config_file_name)
with open(config_path, 'r') as file:  # 1) config
    config_dict = json.load(file)
from SimController import *  # controller
from SimAgentGrid import *  # agent grid

# get back to original folder and run laboratory
sys.path.append('../..')
# append other settings from terminal to config dict


if config_dict["variants_bool"]:
    random_indexes = [84, 95, 147, 10, 80, 36, 46, 25, 44, 58, 55, 109, 134, 34, 91, 124]
    print(f"random_indexes: {random_indexes}")
    # run for each random positon
    positions = [(0, 0), (1, 0), (4, 0), (5, 0), (7, 0), (1, 1), (2, 1), (4, 1), (7, 1), (12, 1), (13, 1), (14, 1), (15, 1), (16, 1), (2, 2), (4, 2), (5, 2), (7, 2), (8, 2), (9, 2) , (14, 2), (0, 3), (1, 3), (2, 3), (3, 3), (5, 3), (7, 3), (9, 3), (10, 3), (14, 3), (15, 3), (0, 4), (3, 4), (5, 4), (6, 4), (8, 4), (10, 4), (11, 4), (12, 4), (15, 4), (16, 4), ( 1, 5), (2, 5), (3, 5), (4, 5), (6, 5), (8, 5), (10, 5), (12, 5), (13, 5), (14, 5), (15, 5), (17, 5), (3, 6), (6, 6), (8, 6), (9, 6), (10, 6), (13, 6), (17, 6), (2, 7), (3, 7), (4, 7), (5, 7), (6, 7), (7, 7), (8, 7), (10, 7), (11, 7), (12, 7), (15, 7), (16, 7), (17, 7), (1, 8), (2, 8), (4, 8), (7, 8), (9, 8), (13, 8), (14, 8), (15, 8), (1, 9), (7, 9), (9, 9), (10, 9), (11, 9), (13, 9), (15, 9), (1, 10), (4, 10), (5, 10), (7, 10), (9, 10), (11, 10), (13, 10), (15, 10), (16, 10), (17, 10), (1, 11), (2, 11), (3, 11), (5, 11), (6, 11), (7, 11), (11, 11), (12, 11), (13, 11), (2, 12), (7, 12), (8, 12), (9, 12), (10, 12), (11, 12), (13, 12), (14, 12), (15, 12), (16, 12), (2, 13), (7, 13), (10, 13), (13, 13), (2, 14), ( 3, 14), (4, 14), (7, 14), (9, 14), (10, 14), (12, 14), (15, 14), (16, 14), (17, 14), (0, 15), (1, 15), (2, 15), (4, 15), (5, 15), (6, 15), (8, 15), (10, 15), (11, 15), (12, 15), (13, 15), (15, 15), (0, 16), (2, 16), (3, 16), (8, 16), (9, 16), (10, 16), (13, 16), (14, 16), (15, 16), (6, 17), (7, 17), (8, 17), (11, 17), (12, 17), (13, 17), (15, 17), (16, 17)]
    for rnd_ind in random_indexes:
        pos = positions[rnd_ind]
        config_dict["initial_position"] = pos
        laboratory = MreLaboratory(config_dict, SimController, SimAgentGrid)
        laboratory.run()
else:

    laboratory = MreLaboratory(config_dict, SimController, SimAgentGrid)
    laboratory.run()
