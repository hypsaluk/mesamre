mesamre
========================

Mesamre is the simulator for multi-agent exploration used in hypsaluk bachelor thesis. It is developed in Python framework Mesa.

In the /mre folder general files of the simulator are located. Apart of model implementation, it contains visualizations and class MreStatistician, which generates output data for simulations.

In /scenarios are folders with scenarios, where each scenario corresponds to different target selection function.

Maps are located in /scenarios/maps.

In a given scenario folder, you can modify parameters of simulation in config.json. Overview of parameters is in /parameters.txt file.

Simulation of a given scenario is then ran using command:

python3 run.py scenarios/[scenario-folder]
