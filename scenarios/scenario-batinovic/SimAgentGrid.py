from mre.grids.MreAgentGrid import *
#from FriendInfo import *

class SimAgentGrid(MreAgentGrid):
    def __init__(self, width, height, scenario_params):
        super().__init__(width, height, scenario_params)
        self.friends_info_dict = {}
        #self._friends_info_dict_inited = False

    """
    def init_friend_dict(self, id, num_agents):
        for i in range(num_agents):
            if i == id:
                continue
            self.friends_info_dict[i] = FriendInfo()
        self.friends_info_dict_inited = True

    def get_friends_info_dict_inited_bool(self):
        return self._friends_info_dict_inited
    """
