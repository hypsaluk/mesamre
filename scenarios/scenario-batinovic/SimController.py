import copy

from mre.MreController import *
from mre.grids.cell_properties.MreECellStatus import *
from mre.grids.cell_properties.MreECellOccupancy import *
from Frontier import *
from Hungarian import *
from WeightCounter import *
from FriendInfo import *


class SimController(MreController):

    def __init__(self, rand_function, scenario_params):
        super().__init__(rand_function, scenario_params)
        u_multiplicative_constant = scenario_params["u_multiplicative_constant"]
        of_multiplicative_constant = scenario_params["of_multiplicative_constant"]
        r = scenario_params["r"]
        r_f = scenario_params["r_f"]
        self._weight_counter = WeightCounter(u_multiplicative_constant, of_multiplicative_constant, r, r_f)

    """
    CONTROL
    """

    def control(self, env_grid, simulator):
        agents = env_grid.get_agents()
        if self._comm_eval == 0:
            received_bool_array = self._comm_evaluator.get_knowledge_received_array_basic(env_grid)
        elif self._comm_eval == 1:
            received_bool_array = self._comm_evaluator.get_knowledge_received_array_poisson(env_grid)
        else:
            print("parameter comm_eval has unsupported value!")
            return
        agents_num = len(agents)
        indexes_list = list(range(agents_num))
        for i in indexes_list:
            agent = agents[i]
            # print(f"CONTROLLING AGENT {agent.unique_id}")
            knowledge_list = self._get_knowledge_received_by_agent(agents, agent.unique_id,
                                                                   received_bool_array)  # decide receive knowledge
            self.update_agent_friends_info(agent, knowledge_list, simulator)  # update knowledge
            self._assign_next_move(agent)  # assign next move

    """
    KNOWLEDGE
    """

    def update_agent_friends_info(self, agent, knowledge_list, simulator):
        a_grid = agent.get_grid()
        for knowledge in knowledge_list:
            friend_id = knowledge.id
            friend_info = FriendInfo()
            friend_info.position = knowledge.position
            friend_info.target_point = knowledge.target_point
            a_grid.friends_info_dict[friend_id] = friend_info  # update friend info in dict
            friends_map = knowledge.grid
            self._update_agent_map(a_grid, friends_map)  # update map
            simulator.inform_agents_received_knowledge_simulator(friend_id, agent.unique_id)

    def _overwrite_cell_grids(self, cell, grid_old, grid_actual):
        """
        overwrite cell in grid old by cell from grid actual
        """
        status = grid_actual.get_cell_prop_val(cell, CELL_STATUS_PROP_ENAME)
        occupancy = grid_actual.get_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME)
        grid_old.set_cell_prop_val(cell, CELL_STATUS_PROP_ENAME, status)
        grid_old.set_cell_prop_val(cell, CELL_OCCUPANCY_PROP_ENAME, occupancy)
        return grid_old

    def _update_agent_map(self, a_grid, friends_map):
        """
            each grid has following layers
            - occupancy
            - status
        """

        """

        #   try to cells, and update frontiers
        #   if no update for frontier, put to blacklist
        #   add new frontier if not on black list
        # if no frontiers to update, finish

        # take frontiers
        frontiers = self.get_frontier_cells(a_grid)
        # for each frontier
        for f in frontiers:
            #  update all 4 neighbours
            nbrs = a_grid.get_neighborhood(f, False, False, 1)
                # for each neighbour
                for n in nbrs:
                # if unknown
                    if a_grid.is_cell_unkown(n):
                        # get friends cell and update
                        friends_cell_dict = a_grid.get_cell_content_dict(n)
                        if friends_cell_dict[CELL_STATUS_PROP_ENAME] != MreECellStatus.UNKNOWN.value:
                            self._overwrite_cell_grids(n, a_grid, friends_map)
                            # if free, add as frontier
                            if friends_cell_dict[CELL_OCCUPANCY_PROP_ENAME] == MreECellOccupancy.FREE.value:
                                frontiers.append(n)
                # else if free and not explored
                    elif (not a_grid.is_cell_explored(n)) and a_grid.is_cell_free(n):
                    #  update if explored
            # each new free cell put to frontiers
            #
        """

        width = a_grid.width
        height = a_grid.height
        # iterate over each cell
        for y in range(height):
            for x in range(width):
                cell = (x, y)
                # if cell in grid1 has lower status than in grid2 -> overwrite
                if self._has_first_higher_status(cell, friends_map, a_grid):
                    self._overwrite_cell_grids(cell, a_grid, friends_map)

    def _has_first_higher_status(self, cell, grid_1, grid_2):
        """
        decides whether cell in grid1 has higher status than in grid2
        """
        if grid_1.is_cell_explored(cell) and (not grid_2.is_cell_explored(cell)):
            return True
        elif grid_1.is_cell_discovered(cell) and (grid_2.is_cell_unknown(cell)):
            return True
        return False

    """ ASSIGN NEXT MOVE """

    def _assign_next_move(self, agent):
        next_move = (0, 0)
        if agent.get_collision_occured() is not None:  # previous move was collision
            coll_id = agent.get_collision_occured()
            # if bumped into agent, forget agents target point
            if coll_id >= 0:
                # forget
                if coll_id in agent.get_grid().friends_info_dict:
                    del agent.get_grid().friends_info_dict[coll_id]
            # replan target
            target_cell = self.get_new_target_cell(agent)
            agent.set_target_cell(target_cell)
            """
            print(f"{agent.unique_id} had collision")
            next_move = agent.get_after_collision_move(self._scenario_params["agent_dodge"])
            if self._scenario_params["replan_tp_after_collision"]:
                print(f"agent {agent.unique_id} wants to reach frontier {target_cell} with distance {agent.get_grid().get_astar_distance(agent.pos, target_cell)}")
            """
            agent.set_collision_occured(None)
        else:  # previous move was not a collision
            self._update_target_cell(agent)  # recompute target cell of agent

        target_cell = agent.get_target_cell()  # compute new move based on TC of agent
        if target_cell is not None:
            path = agent.get_grid().get_astar_path(agent.pos, target_cell)
            if len(path) >= 2:  # if path has no second cell, then just wait
                next_position = path[1]
                next_move = (next_position[0] - agent.pos[0], next_position[1] - agent.pos[1])
                # print(f"agent {agent.unique_id} on position {agent.pos} wants to move to {next_position} to reach frontier {target_cell} with distance {len(path)}")
        agent.set_next_move(next_move)

    def _update_target_cell(self, agent):
        if agent.get_target_cell() is not None:  # if agent is standing on its target cell -> overwrite TS to None
            if agent.pos == agent.get_target_cell() or agent.get_grid().is_cell_explored(agent.get_target_cell()):
                agent.set_target_cell(None)
        if agent.get_target_cell() is None:  # if none target cell -> assign new target cell
            target_cell = self.get_new_target_cell(agent)
            # print(f"agent {agent.unique_id} updated target cell to {target_cell}")
            agent.set_target_cell(target_cell)

    """
    COUNT NEW TARGET CELL
    """

    def get_new_target_cell(self, agent):
        grid = agent.get_grid()
        frontier_cells = self.get_frontier_cells(grid)  # find frontiers
        frontier_cells.sort()
        # print(f"num of frontiers: {len(frontier_cells)}")
        # the amount of frontiers may be lower than number of friends -> consider only x nearest friends
        all_agents_info = self.prepare_agents_info(agent,
                                                   len(frontier_cells))  # prepare agents info(dict{agent_id -> agent info})
        """ agents frontiers weights dict """
        agents_frontier_weights_dict = self._count_agents_frontier_weights_dict(all_agents_info, grid, frontier_cells)
        target_cell = self.get_target_frontier(agent.unique_id, agents_frontier_weights_dict,
                                               frontier_cells)  # assign one of frontiers as target cell
        return target_cell

    def prepare_agents_info(self, agent, num_of_frontiers):
        """ get 1 + (num_of_frontiers-1) agents info """
        all_agents_info = copy.deepcopy(agent.get_grid().friends_info_dict)
        self._keep_only_x_nearest_friends(agent, all_agents_info, (num_of_frontiers - 1))
        # add agent info
        agent_info = FriendInfo()
        agent_info.position = agent.pos
        agent_info.target_point = None
        all_agents_info[agent.unique_id] = agent_info
        return all_agents_info

    def _keep_only_x_nearest_friends(self, agent, friends_info, x):
        grid = agent.get_grid()
        agent_pos = agent.pos
        num_of_agents_to_remove = len(friends_info.items()) - x
        if num_of_agents_to_remove <= 0:
            return
        # friends_info dict(list(friends_info.items())[:x])
        max_distant_agent_id = None
        for i in range(num_of_agents_to_remove):
            max_distance = -1
            for fr_agent_id, fr_agent_info in friends_info.items():  # count furthest agent
                fr_position = fr_agent_info.position
                distance = grid.get_manhattan_distance(agent_pos, fr_position)
                if distance > max_distance:
                    max_distance = distance
                    max_distant_agent_id = fr_agent_id
            # remove furthest agent
            friends_info.pop(max_distant_agent_id)

    def _count_agents_frontier_weights_dict(self, all_agents_info, grid, frontier_cells):
        """ count weights of frontiers for all agents """
        agents_frontier_weights_dict = {}  # dict of key(agent_id) -> value(frontier weights dict)
        for a_id, a_info in all_agents_info.items():
            a_pos = a_info.position
            assigned_points_list = self._get_other_agents_ap_list(all_agents_info, a_id)
            frontier_weights = self._weight_counter.count_frontiers_weights_for_single_agent(a_pos, grid,
                                                                                             frontier_cells,
                                                                                             assigned_points_list)
            agents_frontier_weights_dict[a_id] = frontier_weights
        return agents_frontier_weights_dict

    def _get_other_agents_ap_list(self, agents_info, skipp_agent_id):
        assigned_points_list = []
        for agent_id, agent_info in agents_info.items():
            if agent_id == skipp_agent_id:
                continue
            ap = agent_info.target_point
            if ap is not None:
                assigned_points_list.append(ap)
        return assigned_points_list

    def get_target_frontier(self, agent_id, agents_frontier_weights_dict, frontier_cells):
        """ returns target cell for given agent """

        """
        for a_id, agent_frontier_weights in agents_frontier_weights_dict.items():
            print(f"{a_id}) agents frontier weights {agent_frontier_weights}")
        print("\n")
        """

        hungarian = Hungarian()
        assigned_cells = hungarian.hungarian_assign(agents_frontier_weights_dict, frontier_cells)
        target_frontier_coords = assigned_cells[agent_id]
        return target_frontier_coords
