from enum import Enum

class Frontier:

    def __init__(self):
        self.coords = None
        self.price = None
        self.utility = None
        self.occupancy = None
        self.weight = None
