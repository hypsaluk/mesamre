from Frontier import *
import numpy as np


class WeightCounter:

    def __init__(self, u_multiplicative_constant, of_multiplicative_constant, r, r_f):
        self._u_multiplicative_constant = u_multiplicative_constant
        self._of_multiplicative_constant = of_multiplicative_constant
        self.r = r
        self.r_f = r_f
        print(f"u_multiplicative_constant: {self._u_multiplicative_constant}")
        print(f"of_multiplicative_constant: {self._of_multiplicative_constant}")
        print(f"r: {self.r}")
        print(f"r_f: {self.r_f}")

    """
       COUNTING FRONTIERS WEIGHTS
    """

    def count_frontiers_weights_for_single_agent(self, agent_pos, grid, frontier_cells, assigned_points_list):
        """ counts weights of frontiers for given agent
            counts weights of frontiers for given agent
            input:
                - agent_pos(cell): position of given agent
                - grid(AgentGrid): grid on which the computation is run
                - frontier_cells(): frontier cells in grid
            output:
                - frontier_weights(dict{cell->weight}): weights of frontiers
        """
        frontier_weights = {}
        for frontier_cell in frontier_cells:
            cell_weight = self._count_cell_weight(agent_pos, frontier_cell, grid, assigned_points_list)
            frontier_weights[frontier_cell] = cell_weight
        return frontier_weights

    def _count_cell_cost(self, agent_position, cell, grid):
        distance = grid.get_astar_distance(agent_position, cell)  # simply distance to reach the cell
        return distance

    def _count_cell_utility(self, cell, grid, r):
        if r <= 0:
            return 0
        unexplored_cells_num = 0
        nbrs = grid.get_neighborhood(cell, False, False, r)  # get cells in radius r
        for nbr in nbrs:
            if grid.is_cell_discovered(nbr) and grid.is_cell_free(nbr):  # if given cell is border -> count it
                unexplored_cells_num += 1
        utility = unexplored_cells_num * self._u_multiplicative_constant
        return utility

    """
    def _is_on_coords_an_explored_cell(self, cell, grid):
        if grid.out_of_bounds(cell):
            return False
        return grid.is_cell_explored(cell)
    """

    def _count_cell_occupancy_function(self, j, grid, assigned_points_list, r_f):
        if r_f <= 0:
            return 0
        of = 0
        for ap in assigned_points_list:
            ap_distance = grid.get_manhattan_distance(ap, j)
            if ap_distance > r_f:
                continue
            ap_of_val = self._count_of_val(ap_distance, r_f)
            of += ap_of_val
            #print(f"ap:{ap} ap_distance:{ap_distance} ap_of_val:{ap_of_val}")
        of *= self._of_multiplicative_constant
        #print(f"of: {of}")
        return of

    def _count_of_val(self, distance, r_f):
        of_val = 1 - (float(distance) / float(r_f+1))
        return of_val

    def _count_cell_weight(self, agent_position, cell, grid, assigned_points_list):
        # cost
        cell_cost = self._count_cell_cost(agent_position, cell, grid)
        # utility
        cell_utility = 0
        if self.r > 0:
            cell_utility = self._count_cell_utility(cell, grid, self.r)
        # occupancy function
        cell_of = 0
        if len(assigned_points_list) > 0 and self.r_f > 0:
            cell_of = self._count_cell_occupancy_function(cell, grid, assigned_points_list, self.r_f)
        # count weight as ratio between C, U and OF
        weight = cell_cost - cell_utility + cell_of
        """
        print(f"cell_cost : {cell_cost}")
        if cell_utility != 0:
            print(f"cell_utility: {cell_utility}")
        if cell_of != 0:
            print(f"cell_of: {cell_of}")
        """
        return weight
