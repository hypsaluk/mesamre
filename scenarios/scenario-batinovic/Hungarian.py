import numpy as np
from scipy.optimize import linear_sum_assignment


class Hungarian:

    def __init__(self):
        pass

    def hungarian_assign(self, agents_frontier_weights_dict, frontier_cells):
        """
            in: dict {agent_id -> {dict cell -> weight}}
            out: dict {agent_id -> cell}
        """
        frontier_cells.sort()
        assigned_cells = self._hungarian_assign(agents_frontier_weights_dict, frontier_cells) # dict agent_id -> cel
        return assigned_cells


    def _hungarian_assign(self, agents_frontier_weights_dict, frontier_cells):
        assigned_cells = {}
        # 1) create sorted agent_ids_list
        agent_ids_list = []
        for agent_id, agent_frontier_weights in agents_frontier_weights_dict.items():
            agent_ids_list.append(agent_id)
        agent_ids_list.sort()
        # 2)
        cost_matrix = self._prepare_cost_matrix(agents_frontier_weights_dict, frontier_cells, agent_ids_list)
        row_ind, col_ind = linear_sum_assignment(cost_matrix)
        for worker, task in zip(row_ind, col_ind):
            #print(f"Worker {agent_ids_list[worker]} assigned to frontier {frontier_cells[task]} with cost {cost_matrix[worker, task]}")
            assigned_cells[agent_ids_list[worker]] = frontier_cells[task]
        for agent_id in agent_ids_list:
            if agent_id not in assigned_cells:
                assigned_cells[agent_id] = None
        return assigned_cells


    def _prepare_cost_matrix(self, agents_frontier_weights_dict, frontier_cells, agent_ids_list):
        cost_matrix = []
        for i in range(len(agent_ids_list)):
            agent_id = agent_ids_list[i]
            agent_frontier_weights = agents_frontier_weights_dict[agent_id]
            agent_cost_row = []
            for frontier in frontier_cells:
                agent_cost_row.append(agent_frontier_weights[frontier])
            cost_matrix.append(agent_cost_row)
        cost_matrix_np_array = np.array(cost_matrix)
        return cost_matrix_np_array
